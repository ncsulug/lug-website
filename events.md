---
layout: page
title: Events
permalink: /events/
---

<style>
	/*
	   Gets rid of default table coloring from Jenkins, which would
	   otherwise override the custom colors
	 */
	table tbody tr:nth-child(odd)
	td, table tbody tr:nth-child(odd) th {
	    background-color: transparent;
	}

	.presentation {
		background-color: #ffcccc;
	}

	.dinner {
		background-color: #ccccff;
	}

	.hack-day {
		background-color: #cccccc;
	}

	.day-column {
		width: 20%;
	}

	.type-column { width: 35%;
	}

	.location-column {
		width: 30%;
	}

	.time-column {
		width: 15%;
	}
</style>

For fall 2023, our meetings alternate between presentations and dinner
meetings.

<table width="100%">
  <tr>
    <th class="type-column">Type</th>
    <th class="location-column">Location</th>
    <th class="time-column">Time</th>
  </tr>
  <tr class="presentation">
    <td>Presentation meeting</td>
    <td>Room 3001, Engineering Building 2, or online via <a href="https://meet.jit.si/NCSULUG">Jitsi</a></td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>Dinner meeting</td>
    <td>Varies; see email list</td>
    <td>7-8 PM</td>
  </tr>
</table>

The specific meeting schedule is as follows:

<table width="100%">
  <tr>
    <th class="day-column">Date</th>
    <th class="type-column">Topic</th>
    <th class="location-column">Location</th>
    <th class="time-column">Time</th>
  </tr>
  <tr class="presentation">
    <td>October 20, 2023</td>
    <td>Regex: Can't Live With Them, Can't Live Without Them... or can we?</td>
    <td>Room 3001, Engineering Building 2</td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>October 27, 2023</td>
    <td>Dinner</td>
    <td>TBD</td>
    <td>7-8 PM</td>
  </tr>
  <tr class="presentation">
    <td>November 3, 2023</td>
    <td>Lightning Talks</td>
    <td>Room 3001, Engineering Building 2</td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>November 10, 2023</td>
    <td>Dinner</td>
    <td>TBD</td>
    <td>7-8 PM</td>
  </tr>
  <tr class="presentation">
    <td>November 17, 2023</td>
    <td>TBD</td>
    <td>Room 3001, Engineering Building 2</td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>November 24, 2023</td>
    <td>Dinner (Unoffical, due to university closure)</td>
    <td>TBD</td>
    <td>7-8 PM</td>
  </tr>
  <tr class="dinner">
    <td>December 1, 2023</td>
    <td>Dinner</td>
    <td>TBD</td>
    <td>7-8 PM</td>
  </tr>
</table>
