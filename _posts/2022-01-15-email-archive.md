---
layout: post
title:  Archives Updated
---

Thanks to former POTLUG Edward Anderson, we now have access to many LUG emails
previously thought lost. Using those, we were able to fill out a lot of gaps in
the list of presentations (mostly [without slides][archives], though we did get
a few [with slides][presentations]).

Check out out the new email archive, spanning from 2004 to 2022,
[here][emails]. There's plenty of interesting info and discussion in there,
from tips on sed to discussions of the value E 115.

[archives]: ../archives/
[presentations]: ../presentations/
[emails]: ../email-archive/
