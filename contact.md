---
layout: page
title: Contact
permalink: /contact/
---

### [Discord][discord]

<center>
  <iframe src="https://discord.com/widget?id=811425312050642954&theme=dark" width="350" height="350" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
</center>

### [Matrix][matrix]

[![Chat on Matrix](https://matrix.to/img/matrix-badge.svg)][matrix]

Our self-hosted Matrix instance is bridged to Discord using [Mautrix](https://github.com/mautrix/discord).

### IRC

Internet Relay Chat is the primary medium of communication for the LUG. IRC is
a real-time chatting protocol that dates back to 1988 and is still the
technology of choice among free and open source developers. Our channel is
hosted on Freenode and usually has a few dozen people in the room at any given
time.

Please note that the IRC channel is a public forum and not all of the visitors
in the channel are LUG members or students at NC State University.

    Server: irc.libera.chat
    Port: 6667 | 6697 (SSL)
    Channel: #ncsulug

    Web client:
    https://web.libera.chat/#ncsulug

* If you want something a little more featureful, check out
  [Matrix][matrix], and a [presentation][matrix-presentation] on what it is and
  how to set it up

### Mailing List

    lug@lists.ncsu.edu

    To subscribe, email: lug+subscribe@lists.ncsu.edu

    Archives (as of 2020): https://groups.google.com/a/lists.ncsu.edu/g/lug
    Archives (2004-2022):  https://lug.ncsu.edu/email-archive/
    Archives (1994-2002):  https://groups.google.com/g/ncsu.os.linux

### Postal Mail

    Linux Users Group
    c/o SORC
    Campus Box 7295
    Raleigh, NC 27695

[discord]: https://discord.gg/kep9qRc7nR
[matrix]: https://matrix.to/#/#lug:lug.ncsu.edu
[matrix-presentation]: https://gitlab.com/isharacomix/presentations/-/blob/master/matrix/README.md